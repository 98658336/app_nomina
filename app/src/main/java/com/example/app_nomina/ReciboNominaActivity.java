package com.example.app_nomina;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class ReciboNominaActivity extends AppCompatActivity {
    private EditText editTextRecibo;
    private EditText editTextNombre;
    private EditText editTextHorasTrabajadas;
    private EditText editTextHorasExtras;
    private RadioGroup radioButtonGroup;
    private EditText editTextSubtotal;
    private EditText editTextImpuesto;
    private EditText editTextTotal;
    private Button btnCalcular;
    private Button btnlimpiar;
    private Button btnRegresar;
    private TextView tvUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        tvUsuario = findViewById(R.id.tvUsuario);

        // Recuperar el nombre del trabajador del objeto Intent
        String usuario = getIntent().getStringExtra("USUARIO");

        // Establecer el texto del TextView
        tvUsuario.setText(usuario);



        editTextRecibo = findViewById(R.id.edittextrecibo);
        editTextNombre = findViewById(R.id.edittextnombre);
        editTextHorasTrabajadas = findViewById(R.id.edittexHorast);
        editTextHorasExtras = findViewById(R.id.edittexHorase);
        radioButtonGroup = findViewById(R.id.radioGroup);
        editTextSubtotal = findViewById(R.id.edittextsubtotal);
        editTextImpuesto = findViewById(R.id.editteximpuesto);
        editTextTotal = findViewById(R.id.edittextotal);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnlimpiar = findViewById(R.id.btnlimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Validar los campos de texto
                if (editTextNombre.getText().toString().isEmpty()) {
                    editTextNombre.setError("El nombre es requerido");
                    return;
                }

                if (Integer.parseInt(editTextHorasTrabajadas.getText().toString()) < 0) {
                    editTextHorasTrabajadas.setError("Las horas trabajadas deben ser un número entero positivo");
                    return;
                }

                if (Integer.parseInt(editTextHorasExtras.getText().toString()) < 0) {
                    editTextHorasExtras.setError("Las horas extra deben ser un número entero positivo");
                    return;
                }

                // Obtener los datos del usuario
                String nombre = editTextNombre.getText().toString();
                int horasTrabajadas = Integer.parseInt(editTextHorasTrabajadas.getText().toString());
                int horasExtras = Integer.parseInt(editTextHorasExtras.getText().toString());
                String puesto = radioButtonGroup.getCheckedRadioButtonId() == R.id.radioButton1 ? "Auxiliar" :
                        radioButtonGroup.getCheckedRadioButtonId() == R.id.radioButton2 ? "Albañil" : "Ing. Obra";

                // Crea un objeto Recibo
                Recibo recibo = new Recibo(nombre, puesto, horasTrabajadas, horasExtras);

                // Calcula el recibo
                recibo.calcularSubtotal();
                recibo.calcularImpuesto();
                recibo.calcularTotalAPagar();

                // Actualiza los campos de texto con los datos del recibo
                editTextSubtotal.setText(String.format("%.2f", Recibo.getSubtotal()));
                editTextImpuesto.setText(String.format("%.2f", Recibo.getImpuesto()));
                editTextTotal.setText(String.format("%.2f", Recibo.getTotalAPagar()));
            }
        });

        btnlimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Limpiar los campos de texto
                editTextRecibo.setText("");
                editTextNombre.setText("");
                editTextHorasTrabajadas.setText("");
                editTextHorasExtras.setText("");
                radioButtonGroup.clearCheck();
                editTextSubtotal.setText("");
                editTextImpuesto.setText("");
                editTextTotal.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Regresar a la pantalla anterior
                finish();
            }
        });
    }
}