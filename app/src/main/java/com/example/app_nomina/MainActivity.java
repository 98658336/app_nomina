package com.example.app_nomina;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    public static String usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText txtUsuario = findViewById(R.id.txtUsuario);
        final Button btnIngresar = findViewById(R.id.btnIngresar);
        final Button btnSalir = findViewById(R.id.btnSalir);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario = txtUsuario.getText().toString();

                if (txtUsuario.getText().toString().isEmpty()) {
                    txtUsuario.setError("El nombre de usuario es requerido");
                    return;
                }

                // Iniciar la siguiente pantalla aquí
                Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
                intent.putExtra("USUARIO", usuario);
                startActivity(intent);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}