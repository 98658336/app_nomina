package com.example.app_nomina;

public class Recibo {

    private String nombre;
    private String puesto;
    private int horasTrabajadas;
    private int horasExtras;
    private static double subtotal;
    private static double impuesto;
    private static double totalAPagar;

    public Recibo(String nombre, String puesto, int horasTrabajadas, int horasExtras) {
        this.nombre = nombre;
        this.puesto = puesto;
        this.horasTrabajadas = horasTrabajadas;
        this.horasExtras = horasExtras;
    }

    public void calcularSubtotal() {
        double pagoPorHoraNormal = getPagoPorHora(puesto);
        double pagoPorHoraExtra = getPagoPorHoraExtra(puesto);

        this.subtotal = pagoPorHoraNormal * horasTrabajadas + pagoPorHoraExtra * horasExtras;
    }

    public void calcularImpuesto() {
        this.impuesto = subtotal * 0.16;
    }

    public void calcularTotalAPagar() {
        this.totalAPagar = subtotal - impuesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public int getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(int horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public int getHorasExtras() {
        return horasExtras;
    }

    public void setHorasExtras(int horasExtras) {
        this.horasExtras = horasExtras;
    }

    public static double getSubtotal() {
        return subtotal;
    }

    public static double getImpuesto() {
        return impuesto;
    }

    public static double getTotalAPagar() {
        return totalAPagar;
    }

    private double getPagoPorHora(String puesto) {
        switch (puesto) {
            case "Auxiliar":
                return 50.0;
            case "Albañil":
                return 70.0;
            case "Ing. Obra":
                return 100.0;
            default:
                return 0.0;
        }
    }

    private double getPagoPorHoraExtra(String puesto) {
        switch (puesto) {
            case "Auxiliar":
                return 100.0;
            case "Albañil":
                return 140.0;
            case "Ing. Obra":
                return 200.0;
            default:
                return 0.0;
        }
    }
}